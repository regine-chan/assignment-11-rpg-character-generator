﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;
using ClassLibrary1.Interfaces;
using ClassLibrary1.Services;

namespace Assignment_11___RPG_Character_Generator
{
    public partial class Form1 : Form
    {
        // Initializes a character service
        CharacterService characterService = new CharacterService();
        // Initializes the focused character
        Character FocusedCharacter = null;
        

        public Form1()
        {
            // Sets data sources
            Globals.builder.DataSource = "PC7261\\SQLEXPRESS";
            Globals.builder.InitialCatalog = "RPG_Character_Generator";
            Globals.builder.IntegratedSecurity = true;
            // Initializes components
            InitializeComponent();

        }

        // On form load
        private void Form1_Load(object sender, EventArgs e)
        {
            // Sets lbChars to display the name value of the object it stores
            lbChars.DisplayMember = "name";
            // Updates the listbox with characters from the db
            UpdatelbChars();
            // Displays an empty character creation view
            DisplayCharacterCreation();
        }
       
       
        // Updates characters list box with newest data
        private void UpdatelbChars()
        {
            IEnumerable<Character> collection = characterService.FindAll();
            lbChars.Items.Clear();
            if(collection != null)
            {
                foreach (Character character in collection)
                {
                    lbChars.Items.Add(character);
                }
            }
            
        }
       
        // Displays a character with all attributes
        private void DisplayCharacter(Character character)
        {
            if(character == null)
            {
                return;
            }
            tbName.Text = character.Name;
            cbType.Items.Clear();
            cbType.Items.Add(character.ToString());
            cbType.SelectedIndex = 0;
            lbStaminaValue.Text = character.Stamina.ToString();
            lbIntellectValue.Text = character.Intellect.ToString();
            lbHitPointsValue.Text = character.HitPoints.ToString();
            lbStrengthValue.Text = character.Strength.ToString();
            lbAgilityValue.Text = character.Agility.ToString();
            lbArmorValue.Text = character.Armor.ToString();
            lbMagicResistanceValue.Text = character.MagicRecistance.ToString();

            // Small custom text if character has no buffs
            if(character.Buffs != null)
            {
                if (character.Buffs.Count > 0)
                {
                    lbBuffsValue.Text = "";
                    foreach (string buff in character.Buffs)
                    {
                        lbBuffsValue.Text += buff + " ,";
                    }
                }
                else
                {
                    lbBuffsValue.Text = "You have no life motivation...";
                }
            }
        }

        // Displays a character creation screen when clicked
        private void btNewCharacter_Click(object sender, EventArgs e)
        {
            DisplayCharacterCreation();
        }

        // Function to null all fields the user views
        private void DisplayCharacterCreation()
        {
            lbChars.SelectedItem = null;
            FocusedCharacter = null;
            tbName.Text = "";
            cbType.Items.Clear();
            foreach(string type in Globals.CharacterTypes)
            {
                cbType.Items.Add(type);
            }
            cbType.SelectedIndex = 0;
            lbStaminaValue.Text = "";
            lbIntellectValue.Text = "";
            lbHitPointsValue.Text = "";
            lbStrengthValue.Text = "";
            lbAgilityValue.Text = "";
            lbArmorValue.Text = "";
            lbMagicResistanceValue.Text = "";
            lbBuffsValue.Text = "";
        }

        // Creates an instance of the desired character object and saves it to the db
        private Character CreateCharacterObject()
        {
            switch (cbType.SelectedItem)
            {
                case "Assassian":
                    Assassian assassian = new Assassian(tbName.Text);
                    Save(assassian);
                    return assassian;
                    break;
                case "Barbarian":
                    Barbarian barbarian = new Barbarian(tbName.Text);
                    Save(barbarian);
                    return barbarian;
                    break;
                case "DuneRider":
                    DuneRider duneRider = new DuneRider(tbName.Text);
                    Save(duneRider);
                    return duneRider;
                    break;
                case "FireElementalist":
                    FireElementalist fireElementalist = new FireElementalist(tbName.Text);
                    Save(fireElementalist);
                    return fireElementalist;
                    break;
                case "Forester":
                    Forester forester = new Forester(tbName.Text);
                    Save(forester);
                    return forester;
                    break;
                case "Hunter":
                    Hunter hunter = new Hunter(tbName.Text);
                    Save(hunter);
                    return hunter;
                    break;
                case "KelvinFluxator":
                    KelvinFluxator kelvinFluxator = new KelvinFluxator(tbName.Text);
                    Save(kelvinFluxator);
                    return kelvinFluxator;
                    break;
                case "Mage":
                    Mage mage = new Mage(tbName.Text);
                    Save(mage);
                    return mage;
                    break;
                case "Mender":
                    Mender mender = new Mender(tbName.Text);
                    Save(mender);
                    return mender;
                    break;
                case "Shaman":
                    Shaman shaman = new Shaman(tbName.Text);
                    Save(shaman);
                    return shaman;
                    break;
                case "SharpShooter":
                    SharpShooter sharpShooter = new SharpShooter(tbName.Text);
                    Save(sharpShooter);
                    return sharpShooter;
                    break;
                case "Templar":
                    Templar templar = new Templar(tbName.Text);
                    Save(templar);
                    return templar;
                    break;
                case "Warrior":
                    Warrior warrior = new Warrior(tbName.Text);
                    Save(warrior);
                    return warrior;
                    break;
                default:
                    throw new Exception($"Class {cbType.SelectedItem} not found!");
            }
        }

        // Handles on save click event
        private void btSaveCharacter_Click(object sender, EventArgs e)
        {
            // Empty name results in a prompt to enter a name
            if(tbName.Text == "")
            {
                string message = "You must enter a name";
                string caption = "Error in input";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show(message, caption, buttons);
            } 
            else
            {
                // If the focused character is null then create a new character
                if(FocusedCharacter == null)
                {
                    Character character = CreateCharacterObject();
                    UpdatelbChars();
                    
                    lbChars.SelectedItem = GetCharFromListBoxByName(character.Name);
                    FocusedCharacter = (Character)lbChars.SelectedItem;
                    DisplayCharacter(character);
                    PrintString(character.Introduction());
                }
                // If not update it's name
                else
                {
                    UpdateCharacterName();
                }
            }
        }

        private Character GetCharFromListBoxByName(string name)
        {
            foreach(Character character in lbChars.Items)
            {
                if(character.Name == name)
                {
                    return character;
                }
            }
            return null;
        }

        // Handles on delete clicks
        private void btDeleteCharacter_Click(object sender, EventArgs e)
        {
            if(lbChars.SelectedItem != null)
            {
                Delete((Character)lbChars.SelectedItem);
                UpdatelbChars();
                DisplayCharacterCreation();
            }
        }

        // Tells the characer service to update the characters name
        private void UpdateCharacterName()
        {
            if (lbChars.SelectedItem != null)
            {
                Character character = (Character)lbChars.SelectedItem;
                character.Name = tbName.Text;
                Update(character);
                UpdatelbChars();
            }
        }

        // Prompts the user with an emote
        private void btEmote_Click(object sender, EventArgs e)
        {
            if(FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Emote());
            }
        }

        // Function to create message boxes from strings
        private void PrintString(string PrintThis)
        {
            MessageBox.Show(PrintThis);
        }

        // prompts user with an attack text
        private void btAttack_Click(object sender, EventArgs e)
        {
            if (FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Attack());
                DisplayCharacter(FocusedCharacter);
            }
        }

        // prompts user with a move text
        private void btMove_Click(object sender, EventArgs e)
        {
            if (FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Move());
            }
        }

        // Prompts user with a consume text
        private void btConsume_Click(object sender, EventArgs e)
        {
            if (FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Consume());
                DisplayCharacter(FocusedCharacter);
            }
        }

        // Prompts user with a loot text
        private void btLoot_Click(object sender, EventArgs e)
        {
            if (FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Loot());
            }
        }

        // Prompts user with an introduction text
        private void btIntroduction_Click(object sender, EventArgs e)
        {
            if (FocusedCharacter != null)
            {
                PrintString(FocusedCharacter.Introduction());
            }
        }

        // Passes the delete character to the service
        private void Delete(Character character)
        {
            characterService.Delete(character);
        }

        // Passes the update character to the service
        private void Update(Character character)
        {
            characterService.Update(character);
        }

        // Passes the save character to the service
        private void Save(Character character)
        {
            characterService.Save(character);
        }

        // Displays a new character if the user selects another in the box list
        private void lbChars_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(lbChars != null)
                {
                    FocusedCharacter = (Character)lbChars.SelectedItem;
                    DisplayCharacter(FocusedCharacter);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            
            
        }

        // refreshes characters on click
        private void btRefresh_Click(object sender, EventArgs e)
        {
            UpdatelbChars();
        }
    }
}
