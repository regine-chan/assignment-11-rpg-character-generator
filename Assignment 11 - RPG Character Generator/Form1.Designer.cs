﻿namespace Assignment_11___RPG_Character_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCharacters = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.lbStamina = new System.Windows.Forms.Label();
            this.lbIntellect = new System.Windows.Forms.Label();
            this.lbAgility = new System.Windows.Forms.Label();
            this.lbStrength = new System.Windows.Forms.Label();
            this.lbArmor = new System.Windows.Forms.Label();
            this.lbHitPoints = new System.Windows.Forms.Label();
            this.lbMagicResistance = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbBuffsValue = new System.Windows.Forms.Label();
            this.lbMagicResistanceValue = new System.Windows.Forms.Label();
            this.lbArmorValue = new System.Windows.Forms.Label();
            this.lbStrengthValue = new System.Windows.Forms.Label();
            this.lbAgilityValue = new System.Windows.Forms.Label();
            this.lbIntellectValue = new System.Windows.Forms.Label();
            this.lbStaminaValue = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.btNewCharacter = new System.Windows.Forms.Button();
            this.btDeleteCharacter = new System.Windows.Forms.Button();
            this.btSaveCharacter = new System.Windows.Forms.Button();
            this.btEmote = new System.Windows.Forms.Button();
            this.btAttack = new System.Windows.Forms.Button();
            this.btMove = new System.Windows.Forms.Button();
            this.btConsume = new System.Windows.Forms.Button();
            this.btIntroduction = new System.Windows.Forms.Button();
            this.btLoot = new System.Windows.Forms.Button();
            this.lbChars = new System.Windows.Forms.ListBox();
            this.lbHitPointsValue = new System.Windows.Forms.Label();
            this.lbStrengt = new System.Windows.Forms.Label();
            this.lbBuffs = new System.Windows.Forms.Label();
            this.btRefresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbCharacters
            // 
            this.lbCharacters.Location = new System.Drawing.Point(0, 0);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(100, 23);
            this.lbCharacters.TabIndex = 46;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(172, 49);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(38, 13);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Name:";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(172, 80);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(34, 13);
            this.lbType.TabIndex = 3;
            this.lbType.Text = "Type:";
            // 
            // lbStamina
            // 
            this.lbStamina.AutoSize = true;
            this.lbStamina.Location = new System.Drawing.Point(172, 110);
            this.lbStamina.Name = "lbStamina";
            this.lbStamina.Size = new System.Drawing.Size(48, 13);
            this.lbStamina.TabIndex = 4;
            this.lbStamina.Text = "Stamina:";
            // 
            // lbIntellect
            // 
            this.lbIntellect.AutoSize = true;
            this.lbIntellect.Location = new System.Drawing.Point(172, 146);
            this.lbIntellect.Name = "lbIntellect";
            this.lbIntellect.Size = new System.Drawing.Size(47, 13);
            this.lbIntellect.TabIndex = 5;
            this.lbIntellect.Text = "Intellect:";
            // 
            // lbAgility
            // 
            this.lbAgility.AutoSize = true;
            this.lbAgility.Location = new System.Drawing.Point(172, 184);
            this.lbAgility.Name = "lbAgility";
            this.lbAgility.Size = new System.Drawing.Size(37, 13);
            this.lbAgility.TabIndex = 6;
            this.lbAgility.Text = "Agility:";
            // 
            // lbStrength
            // 
            this.lbStrength.Location = new System.Drawing.Point(0, 0);
            this.lbStrength.Name = "lbStrength";
            this.lbStrength.Size = new System.Drawing.Size(100, 23);
            this.lbStrength.TabIndex = 43;
            // 
            // lbArmor
            // 
            this.lbArmor.AutoSize = true;
            this.lbArmor.Location = new System.Drawing.Point(172, 255);
            this.lbArmor.Name = "lbArmor";
            this.lbArmor.Size = new System.Drawing.Size(37, 13);
            this.lbArmor.TabIndex = 8;
            this.lbArmor.Text = "Armor:";
            // 
            // lbHitPoints
            // 
            this.lbHitPoints.AutoSize = true;
            this.lbHitPoints.Location = new System.Drawing.Point(172, 314);
            this.lbHitPoints.Name = "lbHitPoints";
            this.lbHitPoints.Size = new System.Drawing.Size(55, 13);
            this.lbHitPoints.TabIndex = 9;
            this.lbHitPoints.Text = "Hit Points:";
            // 
            // lbMagicResistance
            // 
            this.lbMagicResistance.AutoSize = true;
            this.lbMagicResistance.Location = new System.Drawing.Point(172, 281);
            this.lbMagicResistance.Name = "lbMagicResistance";
            this.lbMagicResistance.Size = new System.Drawing.Size(95, 13);
            this.lbMagicResistance.TabIndex = 10;
            this.lbMagicResistance.Text = "Magic Resistance:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(289, 46);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 11;
            // 
            // lbBuffsValue
            // 
            this.lbBuffsValue.AutoSize = true;
            this.lbBuffsValue.Location = new System.Drawing.Point(286, 356);
            this.lbBuffsValue.Name = "lbBuffsValue";
            this.lbBuffsValue.Size = new System.Drawing.Size(0, 13);
            this.lbBuffsValue.TabIndex = 29;
            // 
            // lbMagicResistanceValue
            // 
            this.lbMagicResistanceValue.AutoSize = true;
            this.lbMagicResistanceValue.Location = new System.Drawing.Point(286, 281);
            this.lbMagicResistanceValue.Name = "lbMagicResistanceValue";
            this.lbMagicResistanceValue.Size = new System.Drawing.Size(0, 13);
            this.lbMagicResistanceValue.TabIndex = 28;
            // 
            // lbArmorValue
            // 
            this.lbArmorValue.AutoSize = true;
            this.lbArmorValue.Location = new System.Drawing.Point(286, 255);
            this.lbArmorValue.Name = "lbArmorValue";
            this.lbArmorValue.Size = new System.Drawing.Size(0, 13);
            this.lbArmorValue.TabIndex = 26;
            // 
            // lbStrengthValue
            // 
            this.lbStrengthValue.AutoSize = true;
            this.lbStrengthValue.Location = new System.Drawing.Point(286, 221);
            this.lbStrengthValue.Name = "lbStrengthValue";
            this.lbStrengthValue.Size = new System.Drawing.Size(0, 13);
            this.lbStrengthValue.TabIndex = 25;
            // 
            // lbAgilityValue
            // 
            this.lbAgilityValue.AutoSize = true;
            this.lbAgilityValue.Location = new System.Drawing.Point(286, 184);
            this.lbAgilityValue.Name = "lbAgilityValue";
            this.lbAgilityValue.Size = new System.Drawing.Size(0, 13);
            this.lbAgilityValue.TabIndex = 24;
            // 
            // lbIntellectValue
            // 
            this.lbIntellectValue.AutoSize = true;
            this.lbIntellectValue.Location = new System.Drawing.Point(286, 146);
            this.lbIntellectValue.Name = "lbIntellectValue";
            this.lbIntellectValue.Size = new System.Drawing.Size(0, 13);
            this.lbIntellectValue.TabIndex = 23;
            // 
            // lbStaminaValue
            // 
            this.lbStaminaValue.AutoSize = true;
            this.lbStaminaValue.Location = new System.Drawing.Point(286, 110);
            this.lbStaminaValue.Name = "lbStaminaValue";
            this.lbStaminaValue.Size = new System.Drawing.Size(0, 13);
            this.lbStaminaValue.TabIndex = 22;
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(289, 80);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(121, 21);
            this.cbType.TabIndex = 30;
            // 
            // btNewCharacter
            // 
            this.btNewCharacter.Location = new System.Drawing.Point(26, 385);
            this.btNewCharacter.Name = "btNewCharacter";
            this.btNewCharacter.Size = new System.Drawing.Size(75, 23);
            this.btNewCharacter.TabIndex = 31;
            this.btNewCharacter.Text = "New";
            this.btNewCharacter.UseVisualStyleBackColor = true;
            this.btNewCharacter.Click += new System.EventHandler(this.btNewCharacter_Click);
            // 
            // btDeleteCharacter
            // 
            this.btDeleteCharacter.BackColor = System.Drawing.Color.Red;
            this.btDeleteCharacter.ForeColor = System.Drawing.Color.Snow;
            this.btDeleteCharacter.Location = new System.Drawing.Point(175, 385);
            this.btDeleteCharacter.Name = "btDeleteCharacter";
            this.btDeleteCharacter.Size = new System.Drawing.Size(75, 23);
            this.btDeleteCharacter.TabIndex = 32;
            this.btDeleteCharacter.Text = "Delete";
            this.btDeleteCharacter.UseVisualStyleBackColor = false;
            this.btDeleteCharacter.Click += new System.EventHandler(this.btDeleteCharacter_Click);
            // 
            // btSaveCharacter
            // 
            this.btSaveCharacter.BackColor = System.Drawing.Color.Lime;
            this.btSaveCharacter.ForeColor = System.Drawing.Color.White;
            this.btSaveCharacter.Location = new System.Drawing.Point(263, 385);
            this.btSaveCharacter.Name = "btSaveCharacter";
            this.btSaveCharacter.Size = new System.Drawing.Size(75, 23);
            this.btSaveCharacter.TabIndex = 33;
            this.btSaveCharacter.Text = "Save";
            this.btSaveCharacter.UseVisualStyleBackColor = false;
            this.btSaveCharacter.Click += new System.EventHandler(this.btSaveCharacter_Click);
            // 
            // btEmote
            // 
            this.btEmote.Location = new System.Drawing.Point(530, 49);
            this.btEmote.Name = "btEmote";
            this.btEmote.Size = new System.Drawing.Size(75, 23);
            this.btEmote.TabIndex = 34;
            this.btEmote.Text = "Emote";
            this.btEmote.UseVisualStyleBackColor = true;
            this.btEmote.Click += new System.EventHandler(this.btEmote_Click);
            // 
            // btAttack
            // 
            this.btAttack.Location = new System.Drawing.Point(530, 100);
            this.btAttack.Name = "btAttack";
            this.btAttack.Size = new System.Drawing.Size(75, 23);
            this.btAttack.TabIndex = 35;
            this.btAttack.Text = "Attack";
            this.btAttack.UseVisualStyleBackColor = true;
            this.btAttack.Click += new System.EventHandler(this.btAttack_Click);
            // 
            // btMove
            // 
            this.btMove.Location = new System.Drawing.Point(530, 146);
            this.btMove.Name = "btMove";
            this.btMove.Size = new System.Drawing.Size(75, 23);
            this.btMove.TabIndex = 36;
            this.btMove.Text = "Move";
            this.btMove.UseVisualStyleBackColor = true;
            this.btMove.Click += new System.EventHandler(this.btMove_Click);
            // 
            // btConsume
            // 
            this.btConsume.Location = new System.Drawing.Point(530, 199);
            this.btConsume.Name = "btConsume";
            this.btConsume.Size = new System.Drawing.Size(75, 23);
            this.btConsume.TabIndex = 37;
            this.btConsume.Text = "Consume";
            this.btConsume.UseVisualStyleBackColor = true;
            this.btConsume.Click += new System.EventHandler(this.btConsume_Click);
            // 
            // btIntroduction
            // 
            this.btIntroduction.Location = new System.Drawing.Point(530, 308);
            this.btIntroduction.Name = "btIntroduction";
            this.btIntroduction.Size = new System.Drawing.Size(75, 23);
            this.btIntroduction.TabIndex = 39;
            this.btIntroduction.Text = "Introduction";
            this.btIntroduction.UseVisualStyleBackColor = true;
            this.btIntroduction.Click += new System.EventHandler(this.btIntroduction_Click);
            // 
            // btLoot
            // 
            this.btLoot.Location = new System.Drawing.Point(530, 255);
            this.btLoot.Name = "btLoot";
            this.btLoot.Size = new System.Drawing.Size(75, 23);
            this.btLoot.TabIndex = 38;
            this.btLoot.Text = "Loot";
            this.btLoot.UseVisualStyleBackColor = true;
            this.btLoot.Click += new System.EventHandler(this.btLoot_Click);
            // 
            // lbChars
            // 
            this.lbChars.FormattingEnabled = true;
            this.lbChars.Location = new System.Drawing.Point(12, 46);
            this.lbChars.Name = "lbChars";
            this.lbChars.Size = new System.Drawing.Size(120, 329);
            this.lbChars.TabIndex = 40;
            this.lbChars.SelectedIndexChanged += new System.EventHandler(this.lbChars_SelectedIndexChanged);
            // 
            // lbHitPointsValue
            // 
            this.lbHitPointsValue.AutoSize = true;
            this.lbHitPointsValue.Location = new System.Drawing.Point(286, 314);
            this.lbHitPointsValue.Name = "lbHitPointsValue";
            this.lbHitPointsValue.Size = new System.Drawing.Size(0, 13);
            this.lbHitPointsValue.TabIndex = 45;
            // 
            // lbStrengt
            // 
            this.lbStrengt.AutoSize = true;
            this.lbStrengt.Location = new System.Drawing.Point(172, 221);
            this.lbStrengt.Name = "lbStrengt";
            this.lbStrengt.Size = new System.Drawing.Size(47, 13);
            this.lbStrengt.TabIndex = 47;
            this.lbStrengt.Text = "Strength";
            // 
            // lbBuffs
            // 
            this.lbBuffs.AutoSize = true;
            this.lbBuffs.Location = new System.Drawing.Point(175, 355);
            this.lbBuffs.Name = "lbBuffs";
            this.lbBuffs.Size = new System.Drawing.Size(31, 13);
            this.lbBuffs.TabIndex = 48;
            this.lbBuffs.Text = "Buffs";
            // 
            // btRefresh
            // 
            this.btRefresh.BackColor = System.Drawing.Color.Fuchsia;
            this.btRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRefresh.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btRefresh.Location = new System.Drawing.Point(530, 356);
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(258, 82);
            this.btRefresh.TabIndex = 49;
            this.btRefresh.Text = "USELESS REFRESH BUTTON";
            this.btRefresh.UseVisualStyleBackColor = false;
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btRefresh);
            this.Controls.Add(this.lbBuffs);
            this.Controls.Add(this.lbStrengt);
            this.Controls.Add(this.lbHitPointsValue);
            this.Controls.Add(this.lbChars);
            this.Controls.Add(this.btIntroduction);
            this.Controls.Add(this.btLoot);
            this.Controls.Add(this.btConsume);
            this.Controls.Add(this.btMove);
            this.Controls.Add(this.btAttack);
            this.Controls.Add(this.btEmote);
            this.Controls.Add(this.btSaveCharacter);
            this.Controls.Add(this.btDeleteCharacter);
            this.Controls.Add(this.btNewCharacter);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.lbBuffsValue);
            this.Controls.Add(this.lbMagicResistanceValue);
            this.Controls.Add(this.lbArmorValue);
            this.Controls.Add(this.lbStrengthValue);
            this.Controls.Add(this.lbAgilityValue);
            this.Controls.Add(this.lbIntellectValue);
            this.Controls.Add(this.lbStaminaValue);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbMagicResistance);
            this.Controls.Add(this.lbHitPoints);
            this.Controls.Add(this.lbArmor);
            this.Controls.Add(this.lbStrength);
            this.Controls.Add(this.lbAgility);
            this.Controls.Add(this.lbIntellect);
            this.Controls.Add(this.lbStamina);
            this.Controls.Add(this.lbType);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lbCharacters);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbCharacters;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label lbStamina;
        private System.Windows.Forms.Label lbIntellect;
        private System.Windows.Forms.Label lbAgility;
        private System.Windows.Forms.Label lbStrength;
        private System.Windows.Forms.Label lbArmor;
        private System.Windows.Forms.Label lbHitPoints;
        private System.Windows.Forms.Label lbMagicResistance;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbBuffsValue;
        private System.Windows.Forms.Label lbMagicResistanceValue;
        private System.Windows.Forms.Label lbArmorValue;
        private System.Windows.Forms.Label lbStrengthValue;
        private System.Windows.Forms.Label lbAgilityValue;
        private System.Windows.Forms.Label lbIntellectValue;
        private System.Windows.Forms.Label lbStaminaValue;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Button btNewCharacter;
        private System.Windows.Forms.Button btDeleteCharacter;
        private System.Windows.Forms.Button btSaveCharacter;
        private System.Windows.Forms.Button btEmote;
        private System.Windows.Forms.Button btAttack;
        private System.Windows.Forms.Button btMove;
        private System.Windows.Forms.Button btConsume;
        private System.Windows.Forms.Button btIntroduction;
        private System.Windows.Forms.Button btLoot;
        private System.Windows.Forms.ListBox lbChars;
        private System.Windows.Forms.Label lbHitPointsValue;
        private System.Windows.Forms.Label lbStrengt;
        private System.Windows.Forms.Label lbBuffs;
        private System.Windows.Forms.Button btRefresh;
    }
}

