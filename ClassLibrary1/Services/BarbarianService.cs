﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class BarbarianService : IService<Barbarian>
    {
        BarbarianRepository BarbarianRepository = new BarbarianRepository();

        public void Delete(Barbarian obj)
        {
            BarbarianRepository.Delete(obj);
        }

        public IEnumerable<Barbarian> FindAll()
        {
            return BarbarianRepository.FindAll();
        }

        public Barbarian GetOne(int id)
        {
            return BarbarianRepository.GetOne(id);
        }

        public void Save(Barbarian obj)
        {
            BarbarianRepository.Save(obj);
        }

        public void Update(Barbarian obj)
        {
            BarbarianRepository.Update(obj);
        }
    }
}
