﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class ForesterService : IService<Forester>
    {
        ForesterRepository ForesterRepository = new ForesterRepository();

        public void Delete(Forester obj)
        {
            ForesterRepository.Delete(obj);
        }

        public IEnumerable<Forester> FindAll()
        {
            return ForesterRepository.FindAll();
        }

        public Forester GetOne(int id)
        {
            return ForesterRepository.GetOne(id);
        }

        public void Save(Forester obj)
        {
            ForesterRepository.Save(obj);
        }

        public void Update(Forester obj)
        {
            ForesterRepository.Update(obj);
        }
    }
}
