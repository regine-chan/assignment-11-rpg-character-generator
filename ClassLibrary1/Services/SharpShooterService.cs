﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class SharpShooterService : IService<SharpShooter>
    {
        SharpShooterRepository SharpShooterRepository = new SharpShooterRepository();

        public void Delete(SharpShooter obj)
        {
            SharpShooterRepository.Delete(obj);
        }

        public IEnumerable<SharpShooter> FindAll()
        {
            return SharpShooterRepository.FindAll();
        }

        public SharpShooter GetOne(int id)
        {
            return SharpShooterRepository.GetOne(id);
        }

        public void Save(SharpShooter obj)
        {
            SharpShooterRepository.Save(obj);
        }

        public void Update(SharpShooter obj)
        {
            SharpShooterRepository.Update(obj);
        }
    }
}
