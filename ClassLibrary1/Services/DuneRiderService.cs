﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class DuneRiderService : IService<DuneRider>
    {
        DuneRiderRepository DuneRiderRepository = new DuneRiderRepository();

        public void Delete(DuneRider obj)
        {
            DuneRiderRepository.Delete(obj);
        }

        public IEnumerable<DuneRider> FindAll()
        {
            return DuneRiderRepository.FindAll();
        }

        public DuneRider GetOne(int id)
        {
            return DuneRiderRepository.GetOne(id);
        }

        public void Save(DuneRider obj)
        {
            DuneRiderRepository.Save(obj);
        }

        public void Update(DuneRider obj)
        {
            DuneRiderRepository.Update(obj);
        }
 
    }
}
