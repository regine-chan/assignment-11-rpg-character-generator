﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class MageService : IService<Mage>
    {
        MageRepository MageRepository = new MageRepository();

        public void Delete(Mage obj)
        {
            MageRepository.Delete(obj);
        }

        public IEnumerable<Mage> FindAll()
        {
            return MageRepository.FindAll();
        }

        public Mage GetOne(int id)
        {
            return MageRepository.GetOne(id);
        }

        public void Save(Mage obj)
        {
            MageRepository.Save(obj);
        }

        public void Update(Mage obj)
        {
            MageRepository.Update(obj);
        }
    }
}
