﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class WarriorService : IService<Warrior>
    {
        WarriorRepository WarriorRepository = new WarriorRepository();

        public void Delete(Warrior obj)
        {
            WarriorRepository.Delete(obj);
        }

        public IEnumerable<Warrior> FindAll()
        {
            return WarriorRepository.FindAll();
        }

        public Warrior GetOne(int id)
        {
            return WarriorRepository.GetOne(id);
        }

        public void Save(Warrior obj)
        {
            WarriorRepository.Save(obj);
        }

        public void Update(Warrior obj)
        {
            WarriorRepository.Update(obj);
        }
    }
}
