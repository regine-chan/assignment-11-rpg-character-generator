﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class FireElementalistService : IService<FireElementalist>
    {
        FireElementalistRepository FireElementalistRepository = new FireElementalistRepository();

        public void Delete(FireElementalist obj)
        {
            FireElementalistRepository.Delete(obj);
        }

        public IEnumerable<FireElementalist> FindAll()
        {
            return FireElementalistRepository.FindAll();
        }

        public FireElementalist GetOne(int id)
        {
            return FireElementalistRepository.GetOne(id);
        }

        public void Save(FireElementalist obj)
        {
            FireElementalistRepository.Save(obj);
        }

        public void Update(FireElementalist obj)
        {
            FireElementalistRepository.Update(obj);
        }
    }
}
