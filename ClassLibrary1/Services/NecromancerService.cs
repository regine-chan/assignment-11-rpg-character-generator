﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class NecromancerService : IService<Necromancer>
    {
        NecromancerRepository NecromancerRepository = new NecromancerRepository();

        public void Delete(Necromancer obj)
        {
            NecromancerRepository.Delete(obj);
        }

        public IEnumerable<Necromancer> FindAll()
        {
            return NecromancerRepository.FindAll();
        }

        public Necromancer GetOne(int id)
        {
            return NecromancerRepository.GetOne(id);
        }

        public void Save(Necromancer obj)
        {
            NecromancerRepository.Save(obj);
        }

        public void Update(Necromancer obj)
        {
            NecromancerRepository.Update(obj);
        }
    }
}
