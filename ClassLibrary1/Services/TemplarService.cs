﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class TemplarService : IService<Templar>
    {
        TemplarRepository TemplarRepository = new TemplarRepository();

        public void Delete(Templar obj)
        {
            TemplarRepository.Delete(obj);
        }

        public IEnumerable<Templar> FindAll()
        {
            return TemplarRepository.FindAll();
        }

        public Templar GetOne(int id)
        {
            return TemplarRepository.GetOne(id);
        }

        public void Save(Templar obj)
        {
            TemplarRepository.Save(obj);
        }

        public void Update(Templar obj)
        {
            TemplarRepository.Update(obj);
        }
    }
}
