﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class HunterService : IService<Hunter>
    {
        HunterRepository HunterRepository = new HunterRepository();

        public void Delete(Hunter obj)
        {
            HunterRepository.Delete(obj);
        }

        public IEnumerable<Hunter> FindAll()
        {
            return HunterRepository.FindAll();
        }

        public Hunter GetOne(int id)
        {
            return HunterRepository.GetOne(id);
        }

        public void Save(Hunter obj)
        {
            HunterRepository.Save(obj);
        }

        public void Update(Hunter obj)
        {
            HunterRepository.Update(obj);
        }
    }
}
