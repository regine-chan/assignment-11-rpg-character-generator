﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class ShamanService : IService<Shaman>
    {
        ShamanRepository ShamanRepository = new ShamanRepository();

        public void Delete(Shaman obj)
        {
            ShamanRepository.Delete(obj);
        }

        public IEnumerable<Shaman> FindAll()
        {
            return ShamanRepository.FindAll();
        }

        public Shaman GetOne(int id)
        {
            return ShamanRepository.GetOne(id);
        }

        public void Save(Shaman obj)
        {
            ShamanRepository.Save(obj);
        }

        public void Update(Shaman obj)
        {
            ShamanRepository.Update(obj);
        }
    }
}
