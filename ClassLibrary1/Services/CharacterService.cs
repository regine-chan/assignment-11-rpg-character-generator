﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class CharacterService : IService<Character>
    {
        private AssassianService assassianService = new AssassianService();
        private BarbarianService barbarianService = new BarbarianService();
        private DuneRiderService duneRiderService = new DuneRiderService();
        private FireElementalistService fireElementalistService = new FireElementalistService();
        private ForesterService foresterService  = new ForesterService();
        private HunterService hunterService = new HunterService();
        private KelvinFluxatorService kelvinFluxatorService = new KelvinFluxatorService();
        private MageService mageService = new MageService();
        private MenderService menderService = new MenderService();
        private NecromancerService necromancerService = new NecromancerService();
        private RougeService rougeService = new RougeService();
        private ShamanService shamanService = new ShamanService();
        private SharpShooterService sharpShooterService = new SharpShooterService();
        private TemplarService templarService = new TemplarService();
        private WarriorService warriorService = new WarriorService();



        public void Delete(Character character)
        {
            switch (character.ToString())
            {
                case "Assassian":
                    assassianService.Delete((Assassian)character);
                    break;
                case "Barbarian":
                    barbarianService.Delete((Barbarian)character);
                    break;
                case "DuneRider":
                    duneRiderService.Delete((DuneRider)character);
                    break;
                case "FireElementalist":
                    fireElementalistService.Delete((FireElementalist)character);
                    break;
                case "Forester":
                    foresterService.Delete((Forester)character);
                    break;
                case "Hunter":
                    hunterService.Delete((Hunter)character);
                    break;
                case "KelvinFluxator":
                    kelvinFluxatorService.Delete((KelvinFluxator)character);
                    break;
                case "Mage":
                    mageService.Delete((Mage)character);
                    break;
                case "Mender":
                    menderService.Delete((Mender)character);
                    break;
                case "Necromancer":
                    necromancerService.Delete((Necromancer)character);
                    break;
                case "Shaman":
                    shamanService.Delete((Shaman)character);
                    break;
                case "SharpShooter":
                    sharpShooterService.Delete((SharpShooter)character);
                    break;
                case "Templar":
                    templarService.Delete((Templar)character);
                    break;
                case "Warrior":
                    warriorService.Delete((Warrior)character);
                    break;
                default:
                    throw new Exception($"Class {character.ToString()} not found!");
            }
        }

        public IEnumerable<Character> FindAll()
        {
            List<Character> characters = new List<Character>();
            characters.AddRange(assassianService.FindAll());
            characters.AddRange(barbarianService.FindAll());
            characters.AddRange(duneRiderService.FindAll());
            characters.AddRange(fireElementalistService.FindAll());
            characters.AddRange(foresterService.FindAll());
            characters.AddRange(hunterService.FindAll());
            characters.AddRange(kelvinFluxatorService.FindAll());
            characters.AddRange(mageService.FindAll());
            characters.AddRange(menderService.FindAll());
            characters.AddRange(necromancerService.FindAll());
            characters.AddRange(rougeService.FindAll());
            characters.AddRange(shamanService.FindAll());
            characters.AddRange(sharpShooterService.FindAll());
            characters.AddRange(templarService.FindAll());
            characters.AddRange(warriorService.FindAll());
            return characters;
        }

        public Character GetOne(int id)
        {
            throw new NotImplementedException("Method not implemented");
        }

        public void Save(Character character)
        {
            switch (character.ToString())
            {
                case "Assassian":
                    assassianService.Save((Assassian)character);
                    break;
                case "Barbarian":
                    barbarianService.Save((Barbarian)character);
                    break;
                case "DuneRider":
                    duneRiderService.Save((DuneRider)character);
                    break;
                case "FireElementalist":
                    fireElementalistService.Save((FireElementalist)character);
                    break;
                case "Forester":
                    foresterService.Save((Forester)character);
                    break;
                case "Hunter":
                    hunterService.Save((Hunter)character);
                    break;
                case "KelvinFluxator":
                    kelvinFluxatorService.Save((KelvinFluxator)character);
                    break;
                case "Mage":
                    mageService.Save((Mage)character);
                    break;
                case "Mender":
                    menderService.Save((Mender)character);
                    break;
                case "Shaman":
                    shamanService.Save((Shaman)character);
                    break;
                case "SharpShooter":
                    sharpShooterService.Save((SharpShooter)character);
                    break;
                case "Templar":
                    templarService.Save((Templar)character);
                    break;
                case "Warrior":
                    warriorService.Save((Warrior)character);
                    break;
                default:
                    throw new Exception($"Class not found!");
            }
        }

        public void Update(Character character)
        {
            switch (character.ToString())
            {
                case "Assassian":
                    assassianService.Update((Assassian)character);
                    break;
                case "Barbarian":
                    barbarianService.Update((Barbarian)character);
                    break;
                case "DuneRider":
                    duneRiderService.Update((DuneRider)character);
                    break;
                case "FireElementalist":
                    fireElementalistService.Update((FireElementalist)character);
                    break;
                case "Forester":
                    foresterService.Update((Forester)character);
                    break;
                case "Hunter":
                    hunterService.Update((Hunter)character);
                    break;
                case "KelvinFluxator":
                    kelvinFluxatorService.Update((KelvinFluxator)character);
                    break;
                case "Mage":
                    mageService.Update((Mage)character);
                    break;
                case "Mender":
                    menderService.Update((Mender)character);
                    break;
                case "Shaman":
                    shamanService.Update((Shaman)character);
                    break;
                case "SharpShooter":
                    sharpShooterService.Update((SharpShooter)character);
                    break;
                case "Templar":
                    templarService.Update((Templar)character);
                    break;
                case "Warrior":
                    warriorService.Update((Warrior)character);
                    break;
                default:
                    throw new Exception($"Class not found!");
            }
        }
    }
}
