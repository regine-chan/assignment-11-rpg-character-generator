﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class RougeService : IService<Rouge>
    {
        RougeRepository RougeRepository = new RougeRepository();

        public void Delete(Rouge obj)
        {
            RougeRepository.Delete(obj);
        }

        public IEnumerable<Rouge> FindAll()
        {
            return RougeRepository.FindAll();
        }

        public Rouge GetOne(int id)
        {
            return RougeRepository.GetOne(id);
        }

        public void Save(Rouge obj)
        {
            RougeRepository.Save(obj);
        }

        public void Update(Rouge obj)
        {
            RougeRepository.Update(obj);
        }
    }
}
