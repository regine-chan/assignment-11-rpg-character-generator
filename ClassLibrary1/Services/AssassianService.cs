﻿using ClassLibrary1;
using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class AssassianService : IService<Assassian>
    {
        AssassianRepository assassianRepository = new AssassianRepository();

        public void Delete(Assassian obj)
        {
            assassianRepository.Delete(obj);
        }

        public IEnumerable<Assassian> FindAll()
        {
            return assassianRepository.FindAll();
        }

        public Assassian GetOne(int id)
        {
            return assassianRepository.GetOne(id);
        }

        public void Save(Assassian obj)
        {
            assassianRepository.Save(obj);
        }

        public void Update(Assassian obj)
        {
            assassianRepository.Update(obj);
        }
    }
}
