﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class MenderService : IService<Mender>
    {
        MenderRepository MenderRepository = new MenderRepository();

        public void Delete(Mender obj)
        {
            MenderRepository.Delete(obj);
        }

        public IEnumerable<Mender> FindAll()
        {
            return MenderRepository.FindAll();
        }

        public Mender GetOne(int id)
        {
            return MenderRepository.GetOne(id);
        }

        public void Save(Mender obj)
        {
            MenderRepository.Save(obj);
        }

        public void Update(Mender obj)
        {
            MenderRepository.Update(obj);
        }
    }
}
