﻿using ClassLibrary1.Interfaces;
using ClassLibrary1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Services
{
    public class KelvinFluxatorService : IService<KelvinFluxator>
    {
        KelvinFluxatorRepository KelvinFluxatorRepository = new KelvinFluxatorRepository();

        public void Delete(KelvinFluxator obj)
        {
            KelvinFluxatorRepository.Delete(obj);
        }

        public IEnumerable<KelvinFluxator> FindAll()
        {
            return KelvinFluxatorRepository.FindAll();
        }

        public KelvinFluxator GetOne(int id)
        {
            return KelvinFluxatorRepository.GetOne(id);
        }

        public void Save(KelvinFluxator obj)
        {
            KelvinFluxatorRepository.Save(obj);
        }

        public void Update(KelvinFluxator obj)
        {
            KelvinFluxatorRepository.Update(obj);
        }
    }
}
