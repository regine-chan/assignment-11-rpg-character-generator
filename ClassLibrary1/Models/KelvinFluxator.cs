﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public class KelvinFluxator : Mage
    {
        public KelvinFluxator(int id, string name, int stamina, int intellect, int agility, int strength, int armor, int magicRecistance, int hitPoints) : base(id, name, stamina, intellect, agility, strength, armor, magicRecistance, hitPoints)
        {
        }

        public KelvinFluxator(String Name) : base(Name) { }

        public KelvinFluxator() : base() { }

    }
}
