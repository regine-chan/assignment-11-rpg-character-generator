﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public class FireElementalist : Mage
    {
        public FireElementalist(int id, string name, int stamina, int intellect, int agility, int strength, int armor, int magicRecistance, int hitPoints) : base(id, name, stamina, intellect, agility, strength, armor, magicRecistance, hitPoints)
        {
        }

        public FireElementalist(String Name) : base(Name) { }

        public FireElementalist() : base() { }

    }
}
