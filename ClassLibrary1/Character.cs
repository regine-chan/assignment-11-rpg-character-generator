﻿using System;
using System.Collections.Generic;

namespace ClassLibrary1
{
    public abstract class Character : Interact
    {
        protected Character(int id, string name, int stamina, int intellect, int agility, int strength, int armor, int magicRecistance, int hitPoints)
        {
            Id = id;
            Name = name;
            Stamina = stamina;
            Intellect = intellect;
            Agility = agility;
            Strength = strength;
            Armor = armor;
            MagicRecistance = magicRecistance;
            HitPoints = hitPoints;
            // Volatile field, not stored in DB
            this.Buffs = new HashSet<string>();
        }

        protected Character(string Name)
        {
            Random rnd = new Random();
            this.Name = Name;
            this.Stamina = rnd.Next(1, 15);
            this.Intellect = rnd.Next(1, 15);
            this.Agility = rnd.Next(1, 15);
            this.Strength = rnd.Next(1, 15);
            this.Armor = rnd.Next(1, 15);
            this.MagicRecistance = rnd.Next(1, 15);
            this.HitPoints = rnd.Next(1, 15);
            this.Buffs = new HashSet<string>();

        }

        public Character() {
            Random rnd = new Random();
            this.Name = "";
            this.Stamina = rnd.Next(1, 15);
            this.Intellect = rnd.Next(1, 15);
            this.Agility = rnd.Next(1, 15);
            this.Strength = rnd.Next(1, 15);
            this.Armor = rnd.Next(1, 15);
            this.MagicRecistance = rnd.Next(1, 15);
            this.HitPoints = rnd.Next(1, 15);
            this.Buffs = new HashSet<string>();
        }

        public int Id { get; set; }

        public String Name { get; set; }

        public int Stamina { get; set; }

        public int Intellect { get; set; }

        public int Agility { get; set; }

        public int Strength { get; set; }

        public int Armor { get; set; }

        public int MagicRecistance { get; set; }

        public int HitPoints { get; set; }

        public HashSet<String> Buffs { get; set; }

        public string Attack()
        {
            if(this.HitPoints > 0 && this.Buffs.Count == 0)
            {
                this.HitPoints -= 10;
                if(this.HitPoints <= 0)
                {
                    this.Buffs.Add("You died, but how ... ?");
                }
                return $"{this.Name} attacked himself in confusion";
            }
            else if (this.Buffs.Count > 0 && this.HitPoints > 0)
            {
                return $"{this.Name} passed out in confusion";
            }
            return $"Dead characters can't attack?";
        }

        public string Consume()
        {
            this.Buffs.Add("Drunken Brawler (+10 Stamina).");
            this.Stamina += 10;
            return $"{this.Name} consumed 50 gallons of Pale Ale and is now a drunken brawler.";
        }

        public string Emote()
        {
            return $"{this.Name} blew a kiss!";
        }

        public string Introduction()
        {
            return $"Hello, my name is {this.Name}, you look mighty fine today ;) ";
        }

        public string Loot()
        {
            return $"{this.Name} looted the corpse. Only found a dime, tossed it away for good luck.";
        }

        public string Move()
        {
            return $"{this.Name} moved one square inch left.";
        }

        public string Summarize()
        {
            return this.ToString();
        }

        public override string ToString()
        {
            return base.ToString().Substring(14);
        }
    }
}
