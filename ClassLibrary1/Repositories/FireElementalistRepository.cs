﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ClassLibrary1.Repository
{
    class FireElementalistRepository : IRepository<FireElementalist>
    {
        public void Delete(FireElementalist obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM FireElementalist WHERE ID = @FireElementalistID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FireElementalistID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<FireElementalist> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM FireElementalist");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<FireElementalist> FireElementalists = new List<FireElementalist>();
                            while (reader.Read())
                            {
                                FireElementalist FireElementalist = new FireElementalist();
                                FireElementalist.Id = reader.GetInt32(0);
                                FireElementalist.Name = reader.GetString(1);
                                FireElementalist.Stamina = reader.GetInt32(2);
                                FireElementalist.Intellect = reader.GetInt32(3);
                                FireElementalist.Agility = reader.GetInt32(4);
                                FireElementalist.Strength = reader.GetInt32(5);
                                FireElementalist.Armor = reader.GetInt32(6);
                                FireElementalist.MagicRecistance = reader.GetInt32(7);
                                FireElementalist.HitPoints = reader.GetInt32(8);
                                FireElementalists.Add(FireElementalist);
                            }
                            return FireElementalists;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public FireElementalist GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM FireElementalist WHERE ID = @FireElementalistID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                FireElementalist FireElementalist = new FireElementalist();
                                FireElementalist.Id = reader.GetInt32(0);
                                FireElementalist.Name = reader.GetString(1);
                                FireElementalist.Stamina = reader.GetInt32(2);
                                FireElementalist.Intellect = reader.GetInt32(3);
                                FireElementalist.Agility = reader.GetInt32(4);
                                FireElementalist.Strength = reader.GetInt32(5);
                                FireElementalist.Armor = reader.GetInt32(6);
                                FireElementalist.MagicRecistance = reader.GetInt32(7);
                                FireElementalist.HitPoints = reader.GetInt32(8);
                                return FireElementalist;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(FireElementalist obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO FireElementalist (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(FireElementalist obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE FireElementalist SET name = @newName WHERE ID = @FireElementalistID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@FireElementalistID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
