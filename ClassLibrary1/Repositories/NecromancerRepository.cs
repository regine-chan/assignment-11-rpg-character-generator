﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class NecromancerRepository : IRepository<Necromancer>
    {
        public void Delete(Necromancer obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Necromancer WHERE ID = @NecromancerID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@NecromancerID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Necromancer> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Necromancer");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Necromancer> Necromancers = new List<Necromancer>();
                            while (reader.Read())
                            {
                                Necromancer Necromancer = new Necromancer();
                                Necromancer.Id = reader.GetInt32(0);
                                Necromancer.Name = reader.GetString(1);
                                Necromancer.Stamina = reader.GetInt32(2);
                                Necromancer.Intellect = reader.GetInt32(3);
                                Necromancer.Agility = reader.GetInt32(4);
                                Necromancer.Strength = reader.GetInt32(5);
                                Necromancer.Armor = reader.GetInt32(6);
                                Necromancer.MagicRecistance = reader.GetInt32(7);
                                Necromancer.HitPoints = reader.GetInt32(8);
                                Necromancers.Add(Necromancer);
                            }
                            return Necromancers;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public Necromancer GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Necromancer WHERE ID = @NecromancerID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Necromancer Necromancer = new Necromancer();
                                Necromancer.Id = reader.GetInt32(0);
                                Necromancer.Name = reader.GetString(1);
                                Necromancer.Stamina = reader.GetInt32(2);
                                Necromancer.Intellect = reader.GetInt32(3);
                                Necromancer.Agility = reader.GetInt32(4);
                                Necromancer.Strength = reader.GetInt32(5);
                                Necromancer.Armor = reader.GetInt32(6);
                                Necromancer.MagicRecistance = reader.GetInt32(7);
                                Necromancer.HitPoints = reader.GetInt32(8);
                                return Necromancer;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Necromancer obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Necromancer (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Necromancer obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Necromancer SET name = @newName WHERE ID = @NecromancerID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@NecromancerID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
