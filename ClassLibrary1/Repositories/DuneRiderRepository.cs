﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ClassLibrary1.Repository
{
    class DuneRiderRepository : IRepository<DuneRider>
    {
        public void Delete(DuneRider obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM DuneRider WHERE ID = @DuneRiderID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@DuneRiderID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<DuneRider> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM DuneRider");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<DuneRider> DuneRiders = new List<DuneRider>();
                            while (reader.Read())
                            {
                                DuneRider DuneRider = new DuneRider();
                                DuneRider.Id = reader.GetInt32(0);
                                DuneRider.Name = reader.GetString(1);
                                DuneRider.Stamina = reader.GetInt32(2);
                                DuneRider.Intellect = reader.GetInt32(3);
                                DuneRider.Agility = reader.GetInt32(4);
                                DuneRider.Strength = reader.GetInt32(5);
                                DuneRider.Armor = reader.GetInt32(6);
                                DuneRider.MagicRecistance = reader.GetInt32(7);
                                DuneRider.HitPoints = reader.GetInt32(8);
                                DuneRiders.Add(DuneRider);
                            }
                            return DuneRiders;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public DuneRider GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM DuneRider WHERE ID = @DuneRiderID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                DuneRider DuneRider = new DuneRider();
                                DuneRider.Id = reader.GetInt32(0);
                                DuneRider.Name = reader.GetString(1);
                                DuneRider.Stamina = reader.GetInt32(2);
                                DuneRider.Intellect = reader.GetInt32(3);
                                DuneRider.Agility = reader.GetInt32(4);
                                DuneRider.Strength = reader.GetInt32(5);
                                DuneRider.Armor = reader.GetInt32(6);
                                DuneRider.MagicRecistance = reader.GetInt32(7);
                                DuneRider.HitPoints = reader.GetInt32(8);
                                return DuneRider;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(DuneRider obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO DuneRider (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(DuneRider obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE DuneRider SET name = @newName WHERE ID = @DuneRiderID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@DuneRiderID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
