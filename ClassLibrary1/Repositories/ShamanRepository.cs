﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class ShamanRepository : IRepository<Shaman>
    {
        public void Delete(Shaman obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Shaman WHERE ID = @ShamanID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@ShamanID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Shaman> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Shaman");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Shaman> Shamans = new List<Shaman>();
                            while (reader.Read())
                            {
                                Shaman Shaman = new Shaman();
                                Shaman.Id = reader.GetInt32(0);
                                Shaman.Name = reader.GetString(1);
                                Shaman.Stamina = reader.GetInt32(2);
                                Shaman.Intellect = reader.GetInt32(3);
                                Shaman.Agility = reader.GetInt32(4);
                                Shaman.Strength = reader.GetInt32(5);
                                Shaman.Armor = reader.GetInt32(6);
                                Shaman.MagicRecistance = reader.GetInt32(7);
                                Shaman.HitPoints = reader.GetInt32(8);
                                Shamans.Add(Shaman);
                            }
                            return Shamans;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public Shaman GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Shaman WHERE ID = @ShamanID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Shaman Shaman = new Shaman();
                                Shaman.Id = reader.GetInt32(0);
                                Shaman.Name = reader.GetString(1);
                                Shaman.Stamina = reader.GetInt32(2);
                                Shaman.Intellect = reader.GetInt32(3);
                                Shaman.Agility = reader.GetInt32(4);
                                Shaman.Strength = reader.GetInt32(5);
                                Shaman.Armor = reader.GetInt32(6);
                                Shaman.MagicRecistance = reader.GetInt32(7);
                                Shaman.HitPoints = reader.GetInt32(8);
                                return Shaman;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Shaman obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Shaman (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Shaman obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Shaman SET name = @newName WHERE ID = @ShamanID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@ShamanID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
