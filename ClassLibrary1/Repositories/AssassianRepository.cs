﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    public class AssassianRepository : IRepository<Assassian>
    {
        public void Delete(Assassian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Assassian WHERE ID = @assassianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@assassianID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Assassian> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Assassian");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Assassian> assassians = new List<Assassian>();
                            while (reader.Read())
                            {
                                Assassian assassian = new Assassian();
                                assassian.Id = reader.GetInt32(0);
                                assassian.Name = reader.GetString(1);
                                assassian.Stamina = reader.GetInt32(2);
                                assassian.Intellect = reader.GetInt32(3);
                                assassian.Agility = reader.GetInt32(4);
                                assassian.Strength = reader.GetInt32(5);
                                assassian.Armor = reader.GetInt32(6);
                                assassian.MagicRecistance = reader.GetInt32(7);
                                assassian.HitPoints = reader.GetInt32(8);
                                assassians.Add(assassian);
                            }
                            return assassians;
                        }
                    }
                }

                
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            
            return null;
            
        }


        public Assassian GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Assassian WHERE ID = @assassianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Assassian assassian = new Assassian();
                                assassian.Id = reader.GetInt32(0);
                                assassian.Name = reader.GetString(1);
                                assassian.Stamina = reader.GetInt32(2);
                                assassian.Intellect = reader.GetInt32(3);
                                assassian.Agility = reader.GetInt32(4);
                                assassian.Strength = reader.GetInt32(5);
                                assassian.Armor = reader.GetInt32(6);
                                assassian.MagicRecistance = reader.GetInt32(7);
                                assassian.HitPoints = reader.GetInt32(8);
                                return assassian;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Assassian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Assassian (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Assassian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Assassian SET name = @newName WHERE ID = @assassianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@assassianID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            
        }
    }
}
