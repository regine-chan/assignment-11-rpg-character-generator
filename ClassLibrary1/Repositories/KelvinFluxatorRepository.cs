﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class KelvinFluxatorRepository : IRepository<KelvinFluxator>
    {
        public void Delete(KelvinFluxator obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM KelvinFluxator WHERE ID = @KelvinFluxatorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@KelvinFluxatorID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<KelvinFluxator> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM KelvinFluxator");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<KelvinFluxator> KelvinFluxators = new List<KelvinFluxator>();
                            while (reader.Read())
                            {
                                KelvinFluxator KelvinFluxator = new KelvinFluxator();
                                KelvinFluxator.Id = reader.GetInt32(0);
                                KelvinFluxator.Name = reader.GetString(1);
                                KelvinFluxator.Stamina = reader.GetInt32(2);
                                KelvinFluxator.Intellect = reader.GetInt32(3);
                                KelvinFluxator.Agility = reader.GetInt32(4);
                                KelvinFluxator.Strength = reader.GetInt32(5);
                                KelvinFluxator.Armor = reader.GetInt32(6);
                                KelvinFluxator.MagicRecistance = reader.GetInt32(7);
                                KelvinFluxator.HitPoints = reader.GetInt32(8);
                                KelvinFluxators.Add(KelvinFluxator);
                            }
                            return KelvinFluxators;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public KelvinFluxator GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM KelvinFluxator WHERE ID = @KelvinFluxatorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                KelvinFluxator KelvinFluxator = new KelvinFluxator();
                                KelvinFluxator.Id = reader.GetInt32(0);
                                KelvinFluxator.Name = reader.GetString(1);
                                KelvinFluxator.Stamina = reader.GetInt32(2);
                                KelvinFluxator.Intellect = reader.GetInt32(3);
                                KelvinFluxator.Agility = reader.GetInt32(4);
                                KelvinFluxator.Strength = reader.GetInt32(5);
                                KelvinFluxator.Armor = reader.GetInt32(6);
                                KelvinFluxator.MagicRecistance = reader.GetInt32(7);
                                KelvinFluxator.HitPoints = reader.GetInt32(8);
                                return KelvinFluxator;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(KelvinFluxator obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO KelvinFluxator (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(KelvinFluxator obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE KelvinFluxator SET name = @newName WHERE ID = @KelvinFluxatorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@KelvinFluxatorID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
