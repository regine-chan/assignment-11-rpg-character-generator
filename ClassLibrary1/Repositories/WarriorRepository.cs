﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class WarriorRepository : IRepository<Warrior>
    {
        public void Delete(Warrior obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Warrior WHERE ID = @WarriorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@WarriorID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Warrior> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Warrior");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Warrior> Warriors = new List<Warrior>();
                            while (reader.Read())
                            {
                                Warrior Warrior = new Warrior();
                                Warrior.Id = reader.GetInt32(0);
                                Warrior.Name = reader.GetString(1);
                                Warrior.Stamina = reader.GetInt32(2);
                                Warrior.Intellect = reader.GetInt32(3);
                                Warrior.Agility = reader.GetInt32(4);
                                Warrior.Strength = reader.GetInt32(5);
                                Warrior.Armor = reader.GetInt32(6);
                                Warrior.MagicRecistance = reader.GetInt32(7);
                                Warrior.HitPoints = reader.GetInt32(8);
                                Warriors.Add(Warrior);
                            }
                            return Warriors;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public Warrior GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Warrior WHERE ID = @WarriorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Warrior Warrior = new Warrior();
                                Warrior.Id = reader.GetInt32(0);
                                Warrior.Name = reader.GetString(1);
                                Warrior.Stamina = reader.GetInt32(2);
                                Warrior.Intellect = reader.GetInt32(3);
                                Warrior.Agility = reader.GetInt32(4);
                                Warrior.Strength = reader.GetInt32(5);
                                Warrior.Armor = reader.GetInt32(6);
                                Warrior.MagicRecistance = reader.GetInt32(7);
                                Warrior.HitPoints = reader.GetInt32(8);
                                return Warrior;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Warrior obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Warrior (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Warrior obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Warrior SET name = @newName WHERE ID = @WarriorID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@WarriorID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
