﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class HunterRepository : IRepository<Hunter>
    {
        public void Delete(Hunter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Hunter WHERE ID = @HunterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@HunterID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Hunter> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Hunter");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Hunter> Hunters = new List<Hunter>();
                            while (reader.Read())
                            {
                                Hunter Hunter = new Hunter();
                                Hunter.Id = reader.GetInt32(0);
                                Hunter.Name = reader.GetString(1);
                                Hunter.Stamina = reader.GetInt32(2);
                                Hunter.Intellect = reader.GetInt32(3);
                                Hunter.Agility = reader.GetInt32(4);
                                Hunter.Strength = reader.GetInt32(5);
                                Hunter.Armor = reader.GetInt32(6);
                                Hunter.MagicRecistance = reader.GetInt32(7);
                                Hunter.HitPoints = reader.GetInt32(8);
                                Hunters.Add(Hunter);
                            }
                            return Hunters;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public Hunter GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Hunter WHERE ID = @HunterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Hunter Hunter = new Hunter();
                                Hunter.Id = reader.GetInt32(0);
                                Hunter.Name = reader.GetString(1);
                                Hunter.Stamina = reader.GetInt32(2);
                                Hunter.Intellect = reader.GetInt32(3);
                                Hunter.Agility = reader.GetInt32(4);
                                Hunter.Strength = reader.GetInt32(5);
                                Hunter.Armor = reader.GetInt32(6);
                                Hunter.MagicRecistance = reader.GetInt32(7);
                                Hunter.HitPoints = reader.GetInt32(8);
                                return Hunter;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Hunter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Hunter (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Hunter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Hunter SET name = @newName WHERE ID = @HunterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@HunterID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
