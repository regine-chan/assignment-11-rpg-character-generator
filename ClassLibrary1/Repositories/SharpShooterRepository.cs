﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace ClassLibrary1.Repository
{
    class SharpShooterRepository : IRepository<SharpShooter>
    {
        public void Delete(SharpShooter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM SharpShooter WHERE ID = @SharpShooterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@SharpShooterID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<SharpShooter> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM SharpShooter");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<SharpShooter> SharpShooters = new List<SharpShooter>();
                            while (reader.Read())
                            {
                                SharpShooter SharpShooter = new SharpShooter();
                                SharpShooter.Id = reader.GetInt32(0);
                                SharpShooter.Name = reader.GetString(1);
                                SharpShooter.Stamina = reader.GetInt32(2);
                                SharpShooter.Intellect = reader.GetInt32(3);
                                SharpShooter.Agility = reader.GetInt32(4);
                                SharpShooter.Strength = reader.GetInt32(5);
                                SharpShooter.Armor = reader.GetInt32(6);
                                SharpShooter.MagicRecistance = reader.GetInt32(7);
                                SharpShooter.HitPoints = reader.GetInt32(8);
                                SharpShooters.Add(SharpShooter);
                            }
                            return SharpShooters;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public SharpShooter GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM SharpShooter WHERE ID = @SharpShooterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SharpShooter SharpShooter = new SharpShooter();
                                SharpShooter.Id = reader.GetInt32(0);
                                SharpShooter.Name = reader.GetString(1);
                                SharpShooter.Stamina = reader.GetInt32(2);
                                SharpShooter.Intellect = reader.GetInt32(3);
                                SharpShooter.Agility = reader.GetInt32(4);
                                SharpShooter.Strength = reader.GetInt32(5);
                                SharpShooter.Armor = reader.GetInt32(6);
                                SharpShooter.MagicRecistance = reader.GetInt32(7);
                                SharpShooter.HitPoints = reader.GetInt32(8);
                                return SharpShooter;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(SharpShooter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO SharpShooter (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(SharpShooter obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE SharpShooter SET name = @newName WHERE ID = @SharpShooterID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@SharpShooterID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
