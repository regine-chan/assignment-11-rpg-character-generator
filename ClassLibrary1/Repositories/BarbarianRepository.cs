﻿using ClassLibrary1.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ClassLibrary1.Repository
{
    class BarbarianRepository : IRepository<Barbarian>
    {
        public void Delete(Barbarian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM Barbarian WHERE ID = @BarbarianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@BarbarianID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public IEnumerable<Barbarian> FindAll()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT *");
                    sb.Append("FROM Barbarian");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            List<Barbarian> Barbarians = new List<Barbarian>();
                            while (reader.Read())
                            {
                                Barbarian Barbarian = new Barbarian();
                                Barbarian.Id = reader.GetInt32(0);
                                Barbarian.Name = reader.GetString(1);
                                Barbarian.Stamina = reader.GetInt32(2);
                                Barbarian.Intellect = reader.GetInt32(3);
                                Barbarian.Agility = reader.GetInt32(4);
                                Barbarian.Strength = reader.GetInt32(5);
                                Barbarian.Armor = reader.GetInt32(6);
                                Barbarian.MagicRecistance = reader.GetInt32(7);
                                Barbarian.HitPoints = reader.GetInt32(8);
                                Barbarians.Add(Barbarian);
                            }
                            return Barbarians;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;

        }


        public Barbarian GetOne(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT FROM Barbarian WHERE ID = @BarbarianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Barbarian Barbarian = new Barbarian();
                                Barbarian.Id = reader.GetInt32(0);
                                Barbarian.Name = reader.GetString(1);
                                Barbarian.Stamina = reader.GetInt32(2);
                                Barbarian.Intellect = reader.GetInt32(3);
                                Barbarian.Agility = reader.GetInt32(4);
                                Barbarian.Strength = reader.GetInt32(5);
                                Barbarian.Armor = reader.GetInt32(6);
                                Barbarian.MagicRecistance = reader.GetInt32(7);
                                Barbarian.HitPoints = reader.GetInt32(8);
                                return Barbarian;
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

            return null;
        }

        public void Save(Barbarian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("INSERT INTO Barbarian (name, stamina, intellect, agility, strength, armor, magicResistance, hitPoints) ");
                    sb.Append($"VALUES (@name, @stamina, @intellect, @agility, @strength, @armor, @magicResistance, @hitPoints)");
                    string sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", obj.Name);
                        command.Parameters.AddWithValue("stamina", obj.Stamina);
                        command.Parameters.AddWithValue("intellect", obj.Intellect);
                        command.Parameters.AddWithValue("agility", obj.Agility);
                        command.Parameters.AddWithValue("strength", obj.Strength);
                        command.Parameters.AddWithValue("armor", obj.Armor);
                        command.Parameters.AddWithValue("magicResistance", obj.MagicRecistance);
                        command.Parameters.AddWithValue("hitPoints", obj.HitPoints);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        public void Update(Barbarian obj)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Globals.builder.ConnectionString))
                {
                    connection.Open();

                    string sql = "UPDATE Barbarian SET name = @newName WHERE ID = @BarbarianID";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newName", obj.Name);
                        command.Parameters.AddWithValue("@BarbarianID", obj.Id);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }

        }
    }
}
