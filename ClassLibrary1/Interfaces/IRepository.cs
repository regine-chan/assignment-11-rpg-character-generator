﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> FindAll();

        T GetOne(int id);

        void Save(T obj);

        void Update(T obj);

        void Delete(T obj);
        
    }
}
