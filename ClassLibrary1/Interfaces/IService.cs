﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Interfaces
{
    public interface IService<T>
    {
        IEnumerable<T> FindAll();

        T GetOne(int id);

        void Delete(T obj);

        void Update(T obj);

        void Save(T obj);
    }
}
