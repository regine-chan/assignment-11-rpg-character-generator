﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface Interact
    {
        string Emote();

        string Attack();

        string Move();

        string Consume();

        string Loot();

        string Introduction();

        string Summarize();
    }
}
