USE [master]
GO
/****** Object:  Database [RPG_Character_Generator]    Script Date: 8/20/2020 1:58:50 PM ******/
CREATE DATABASE [RPG_Character_Generator]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RPG_Character_Generator', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPG_Character_Generator.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RPG_Character_Generator_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\RPG_Character_Generator_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RPG_Character_Generator] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RPG_Character_Generator].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RPG_Character_Generator] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET ARITHABORT OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RPG_Character_Generator] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RPG_Character_Generator] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RPG_Character_Generator] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RPG_Character_Generator] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RPG_Character_Generator] SET  MULTI_USER 
GO
ALTER DATABASE [RPG_Character_Generator] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RPG_Character_Generator] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RPG_Character_Generator] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RPG_Character_Generator] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RPG_Character_Generator] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RPG_Character_Generator] SET QUERY_STORE = OFF
GO
USE [RPG_Character_Generator]
GO
/****** Object:  Table [dbo].[Assassian]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assassian](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Assassian] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Barbarian]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Barbarian](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Barbarian] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DuneRider]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DuneRider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_DuneRider] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FireElementalist]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FireElementalist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_FireElementalist] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Forester]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forester](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Forester] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hunter]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hunter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Hunter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KelvinFluxator]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KelvinFluxator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_KelvinFluxator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mage]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Mage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mender]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mender](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Mender] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Necromancer]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Necromancer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Necromancer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rouge]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rouge](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Rouge] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shaman]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shaman](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Shaman] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SharpShooter]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharpShooter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_SharpShooter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Templar]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Templar](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Templar] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warrior]    Script Date: 8/20/2020 1:58:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warrior](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stamina] [int] NULL,
	[intellect] [int] NULL,
	[agility] [int] NULL,
	[strength] [int] NULL,
	[armor] [int] NULL,
	[magicResistance] [int] NULL,
	[hitPoints] [int] NULL,
 CONSTRAINT [PK_Warrior] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [RPG_Character_Generator] SET  READ_WRITE 
GO
